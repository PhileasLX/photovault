package fr.utt.if26.photovault;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;

import java.util.ArrayList;

public class PhotoAdapter extends ArrayAdapter<PV_Photo> {
    ArrayList<PV_Photo> photos;
    Context contexte;
    int ressource;

    public PhotoAdapter(Context context, int resource, ArrayList<PV_Photo> data) {
        super(context, resource, data);
        this.photos = data;
        this.contexte = context;
        this.ressource = resource;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = ((Activity) contexte).getLayoutInflater();
        View v = inflater.inflate(ressource, parent, false);

        PV_Photo photo = photos.get(position);

        ImageView imageView = (ImageView) v.findViewById(R.id.imageView_photo);
        imageView.setImageBitmap(photo.getReducedImage());
        return v;
    }
}