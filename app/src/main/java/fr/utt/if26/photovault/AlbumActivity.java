package fr.utt.if26.photovault;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.PopupMenu;
import android.widget.Toast;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class AlbumActivity extends AppCompatActivity {

    static final int REQUEST_IMAGE_CAPTURE = 1;
    static final int GALLERY_REQUEST = 2;

    Album album;
    String currentPhotoPath;
    Menu menu;
    DB_Persistance db;
    PhotoAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_album);

        db = new DB_Persistance(this, DB_Persistance.DATABASE_NAME, null, DB_Persistance.DATABASE_VERSION);

        final Intent intent = getIntent();

        this.album = new Album(intent.getStringExtra("AlbumName"), intent.getIntExtra("AlbumID", 0));
        this.album.setPhotos(db.getPhotosFromAlbum(intent.getIntExtra("AlbumID", 0)));
        this.setTitle(this.album.getName());

        // Used to delete all temporary files created
        //deleteAllFiles();

        final GridView gridView = (GridView) findViewById(R.id.album_gridView);
        adapter = new PhotoAdapter(this, R.layout.photo, album.getPhotos());
        gridView.setAdapter(adapter);

        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(AlbumActivity.this, FullScreenPhotoActivity.class);
                intent.putExtra("PhotoID", album.getPhoto(position).getId());
                intent.putExtra("AlbumID", album.getId());
                intent.putExtra("AlbumName", album.getName());
                startActivity(intent);
            }
        });
        gridView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                final int itemPosition = position;
                PopupMenu popup = new PopupMenu(AlbumActivity.this, gridView.getChildAt(itemPosition));
                popup.getMenuInflater().inflate(R.menu.contextmenu_photo, popup.getMenu());

                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.contextmenu_photo_delete:
                                db.deletePhoto(album.getPhoto(itemPosition).getId());
                                album.deletePhoto(itemPosition);
                                Toast.makeText(AlbumActivity.this, "Photo supprimée.", Toast.LENGTH_LONG).show();
                                adapter.notifyDataSetChanged();
                                return true;

                            default:
                                return true;
                        }
                    }
                });
                popup.show();
                return true;
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(AlbumActivity.this, VaultActivity.class);
        startActivity(intent);
    }

    @Override
    protected void onResume() {
        super.onResume();
        this.setTitle(album.getName());
        adapter.notifyDataSetChanged();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_album, menu);
        this.menu = menu;
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_album_addPhoto:
                showAlertDialogAddPhotoChoice();
                return true;
            case R.id.menu_album_parameters:
                showAlertDialogEditAlbumChoice();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }

    }

    public void showAlertDialogAddPhotoChoice() {
        // setup the alert builder
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Ajouter une photo");

        // add the buttons
        builder.setPositiveButton("Prendre une photo", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dispatchTakePictureIntent();
            }
        });
        builder.setNegativeButton("Récupérer une image", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
                photoPickerIntent.setType("image/*");
                startActivityForResult(photoPickerIntent, GALLERY_REQUEST);
            }
        });

        // create and show the alert dialog
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                // Error occurred while creating the File
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                Uri photoURI = FileProvider.getUriForFile(AlbumActivity.this,
                        "fr.utt.if26.photovault.fileProvider",
                        photoFile);
                currentPhotoPath = photoFile.getAbsolutePath();
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
            }
        }
    }

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);

        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );
        currentPhotoPath = image.getAbsolutePath();
        return image;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK){
            PV_Photo photo = new PV_Photo(BitmapFactory.decodeFile(currentPhotoPath));
            album.addPhoto(photo);
            db.addPhoto(photo, album.getId());
        }
        else
            if(requestCode == GALLERY_REQUEST && resultCode == RESULT_OK){
                Uri selectedImage = data.getData();
                try {
                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), selectedImage);
                    PV_Photo photo = new PV_Photo(bitmap);
                    album.addPhoto(photo);
                    db.addPhoto(photo, album.getId());
                } catch (IOException e) {
                    Toast.makeText(this, "Something went wrong", Toast.LENGTH_LONG).show();
                }
            }
        deleteAllFiles();
    }

    private void deleteAllFiles(){
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);

        File[] files = storageDir.listFiles();
        for( int i=0 ; i < files.length ; i++){
            File currentFile = files[i];
            currentFile.delete();
        }
    }

    public void showAlertDialogEditAlbumChoice() {
        // setup the alert builder
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Modifier l'album");

        // add the buttons
        builder.setPositiveButton("Changer le nom", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                showAlertDialogEditAlbum(db.EDIT_NAME);
            }
        });
        builder.setNegativeButton("Changer le mot de passe", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                showAlertDialogEditAlbum(db.EDIT_PASSWORD);
            }
        });

        // create and show the alert dialog
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    public void showAlertDialogEditAlbum(short arg) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();

        final View dialogView = inflater.inflate(R.layout.dialog_albumadd, null);

        builder.setView(dialogView);

        final EditText et_name = (EditText) dialogView.findViewById(R.id.et_newAlbumName);
        final EditText et_password = (EditText) dialogView.findViewById(R.id.et_newAlbumPassword);

        final int albumId = album.getId();

        if(arg == db.EDIT_NAME){
            et_name.setHint("Nouveau nom");
            et_password.setHint("Mot de passe");
            builder.setTitle("Modifier le nom");
            builder.setPositiveButton("Valider", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    final String newName = et_name.getText().toString();
                    if(!newName.equals("")) {
                        if (db.canAccessAlbum(et_password.getText().toString(), albumId)) {
                            Toast.makeText(getApplicationContext(), "Album " + album.getName() + " modifié en " + newName, Toast.LENGTH_LONG).show();
                            db.editAlbum(albumId, newName, db.EDIT_NAME);
                            album.setName(newName);
                            AlbumActivity.this.setTitle(newName);
                        }else{
                            Toast.makeText(getApplicationContext(), "Mauvais mot de passe.", Toast.LENGTH_LONG).show();
                        }
                    }
                    else{
                        Toast.makeText(getApplicationContext(), "Entrez un nouveau nom.", Toast.LENGTH_LONG).show();
                    }

                }
            });
        }if(arg == db.EDIT_PASSWORD){
            et_name.setHint("Nouveau mot de passe");
            et_password.setHint("Mot de passe");
            et_name.setInputType(et_password.getInputType());
            builder.setTitle("Modifier le mot de passe");

            builder.setPositiveButton("Valider", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    final String newPass = et_name.getText().toString();
                    if (db.canAccessAlbum(et_password.getText().toString(), albumId)) {
                        Toast.makeText(getApplicationContext(), "Mot de passe changé.", Toast.LENGTH_LONG).show();
                        db.editAlbum(albumId, newPass, db.EDIT_PASSWORD);
                    }else{
                        Toast.makeText(getApplicationContext(), "Mauvais mot de passe.", Toast.LENGTH_LONG).show();
                    }



                }
            });
        }


        builder.setNegativeButton("Annuler", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
            }
        });

        AlertDialog dialog = builder.create();
        dialog.show();
    }
}
