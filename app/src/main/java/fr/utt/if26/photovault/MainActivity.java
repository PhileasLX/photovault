package fr.utt.if26.photovault;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    Button btn_login;
    EditText et_password;
    DB_Persistance db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        db = new DB_Persistance(this, DB_Persistance.DATABASE_NAME, null, DB_Persistance.DATABASE_VERSION);

        btn_login = (Button) findViewById(R.id.btn_login);
        et_password = findViewById(R.id.et_password);
        et_password.setHint("Mot de passe");

        // If there is no password for the vault: A new EditText is inserted to confirm the new
        // password entered
        if(!db.isUserAlreadyRegistered()){
            final EditText et_newPassword = new EditText(this);
            et_newPassword.setInputType(et_password.getInputType());
            et_newPassword.setHint("Confirmation");
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            params.gravity = Gravity.CENTER_HORIZONTAL;
            et_newPassword.setLayoutParams(params);
            LinearLayout layout = findViewById(R.id.linearLayout_main);
            layout.addView(et_newPassword, 2);

            btn_login.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String password = et_password.getText().toString();
                    if(et_newPassword.getText().toString().equals(password)){
                        if(password.equals(""))
                            Toast.makeText(MainActivity.this, "Entrez un mot de passe.", Toast.LENGTH_LONG).show();
                        else{
                            db.createVault(password);
                            Intent intent = new Intent(MainActivity.this, VaultActivity.class);
                            startActivity(intent);
                        }
                    }else{
                        Toast.makeText(MainActivity.this, "Mots de passes différents.", Toast.LENGTH_LONG).show();
                    }
                }
            });

        }
        else {
            btn_login.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(db.canAccessVault(et_password.getText().toString())){
                        Intent intent = new Intent(MainActivity.this, VaultActivity.class);
                        startActivity(intent);
                    }
                    else
                        Toast.makeText(MainActivity.this, "Mauvais mot de passe.", Toast.LENGTH_LONG).show();

                }
            });
        }


    }

    @Override
    protected void onResume() {
        super.onResume();
        et_password.setText("");
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(MainActivity.this, MainActivity.class);
        startActivity(intent);
    }
}
