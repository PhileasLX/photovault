package fr.utt.if26.photovault;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.nio.ByteBuffer;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.Arrays;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

public class DB_Persistance extends SQLiteOpenHelper {
    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "photovault.db";
    public static final String DEFAULT_PASSWORD = "-1";
    public static final short EDIT_NAME = 0;
    public static final short EDIT_PASSWORD = 1;

    private static final String TABLE_PHOTO = "photo";
    private static final String PHOTO_ID = "photo_id";
    private static final String PHOTO_ALBUM_ID = "photo_album_id";
    private static final String PHOTO_DATA = "photo_data";

    private static final String TABLE_ALBUM = "album";
    private static final String ALBUM_ID = "album_id";
    private static final String ALBUM_NAME = "album_name";
    private static final String ALBUM_PASSWORD = "album_password";

    private static final String TABLE_USER = "user";
    private static final String USER_PASSWORD = "user_password";

    public DB_Persistance(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        final String table_photo_create =
                "CREATE TABLE " + TABLE_PHOTO + "("
                        + PHOTO_ID + " INTEGER primary key not null,"
                        + PHOTO_ALBUM_ID + " INTEGER not null,"
                        + PHOTO_DATA + " BLOB not null,"
                        + "FOREIGN KEY(" + PHOTO_ALBUM_ID + ") REFERENCES " + TABLE_ALBUM +"(" + ALBUM_ID + ")"
                        + ")";

        final String table_album_create =
                "CREATE TABLE " + TABLE_ALBUM + "("
                        + ALBUM_ID + " INTEGER primary key autoincrement,"
                        + ALBUM_NAME + " TEXT not null,"
                        + ALBUM_PASSWORD + " BLOB"
                        + ")";

        final String table_user_create =
                "CREATE TABLE " + TABLE_USER + "("
                        + USER_PASSWORD + " BLOB not null"
                        + ")";

        db.execSQL(table_user_create);
        db.execSQL(table_album_create);
        db.execSQL(table_photo_create);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE " + TABLE_USER);
        db.execSQL("DROP TABLE " + TABLE_ALBUM);
        db.execSQL("DROP TABLE " + TABLE_PHOTO);
        this.onCreate(db);
        db.close();
    }

    public void addPhoto(PV_Photo photo, int albumId) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(PHOTO_ID, photo.getId());
        values.put(PHOTO_ALBUM_ID, albumId);
        // To ensure that the photo is secured, it is encrypted and then put in the database.
        values.put(PHOTO_DATA, encrypt(photo.getImageBytes(), photo.getId()));

        // Inserting Row
        db.insert(TABLE_PHOTO, null, values);

        db.close();
    }

    public int addAlbum(Album album, String password){
        SQLiteDatabase db = this.getWritableDatabase();

        if(password.equals(""))
            password = DEFAULT_PASSWORD;

        ContentValues values = new ContentValues();
        values.put(ALBUM_NAME, album.getName());
        values.put(ALBUM_PASSWORD, this.hashPassword(password));

        // Inserting Row
        int id = (int) db.insert(TABLE_ALBUM, null, values);

        db.close();
        return id;
    }

    public void editAlbum(int id, String value, short arg){
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        if(arg == EDIT_NAME)
            values.put(ALBUM_NAME, value);
        if(arg == EDIT_PASSWORD){
            if(value.equals(""))
                value = DEFAULT_PASSWORD;
            values.put(ALBUM_PASSWORD, this.hashPassword(value));
        }

        db.update(TABLE_ALBUM, values, ALBUM_ID+"="+id, null);
        db.close();
    }

    public void deleteAlbum(int id){
        SQLiteDatabase db = this.getWritableDatabase();

        // Delete every photo in the album
        db.delete(TABLE_PHOTO, PHOTO_ALBUM_ID + "=?", new String[]{Integer.toString(id)});

        // Delete the album
        db.delete(TABLE_ALBUM, ALBUM_ID + "=?", new String[]{Integer.toString(id)});

        db.close();
    }

    public void deletePhoto(long id){
        SQLiteDatabase db = this.getWritableDatabase();

        db.delete(TABLE_PHOTO, PHOTO_ID + "=?", new String[]{Long.toString(id)});

        db.close();
    }

    public ArrayList<Album> getAlbums(){
        ArrayList<Album> albums = new ArrayList<Album>();

        String selectQuery = "SELECT  * FROM " + TABLE_ALBUM;

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        while(cursor.moveToNext()){
            Album album = new Album(cursor.getString(1), cursor.getInt(0));
            Log.i("getAlbums", "Album name:" + album.getName() + " ; Album id:" + cursor.getInt(0));
            albums.add(album);
        }
        cursor.close();
        db.close();
        Log.i("getAlbums", "albums.size:" + albums.size());

        return albums;
    }

    public ArrayList<PV_Photo> getPhotosFromAlbum(int id){
        ArrayList<PV_Photo> photos = new ArrayList<>();

        if(id < 1)
            return photos;

        String selectQuery = "SELECT  * FROM " + TABLE_PHOTO + " WHERE " + PHOTO_ALBUM_ID + "=" + id;

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        while(cursor.moveToNext()){
            PV_Photo photo = new PV_Photo(decrypt(cursor.getBlob(2), cursor.getLong(0)), cursor.getLong(0));

            photos.add(photo);
        }
        cursor.close();
        db.close();

        return photos;
    }

    public byte[] hashPassword(String password){
        byte[] hash = null;
        try {
            MessageDigest md = MessageDigest.getInstance("SHA-256");
            byte[] bytes = password.getBytes("UTF-8");
            md.update(bytes, 0, bytes.length);
            hash = md.digest();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return hash;
    }

    public byte[] hashPassword(byte[] bytes){
        byte[] hash = null;
        try {
            MessageDigest md = MessageDigest.getInstance("SHA-256");
            md.update(bytes, 0, bytes.length);
            hash = md.digest();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return hash;
    }

    public Boolean canAccessAlbum(String password, int id){
        SQLiteDatabase db = this.getReadableDatabase();

        if(password.equals(""))
            password = DEFAULT_PASSWORD;

        Cursor cursor = db.query(TABLE_ALBUM, new String[] { ALBUM_PASSWORD }, ALBUM_ID + "=?",
                new String[] { Integer.toString(id)}, null, null, null, null);
        if (cursor != null)
            cursor.moveToFirst();
        if(Arrays.equals(cursor.getBlob(0), this.hashPassword(password)))
            return true;
        return false;
    }

    public byte[] getPhoto(long id) {
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(TABLE_PHOTO, new String[] { PHOTO_DATA }, PHOTO_ID + "=?",
                new String[] { Long.toString(id)}, null, null, null, null);
        if (cursor != null)
            cursor.moveToFirst();
        return decrypt(cursor.getBlob(0), id);
    }

    private byte[] getUserPassword(){
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(TABLE_USER, new String[] { USER_PASSWORD }, null,
                null , null, null, null, null);
        if (cursor != null)
            cursor.moveToFirst();
        return cursor.getBlob(0);
        //return this.hashPassword(DEFAULT_PASSWORD);
    }

    public boolean isUserAlreadyRegistered(){
        SQLiteDatabase db = this.getReadableDatabase();

        long count = DatabaseUtils.queryNumEntries(db, TABLE_USER);
        Log.i("isUserAlreadyRegistered", "Count: " + count);
        db.close();
        return (count != 0);
    }

    private SecretKeySpec generateKey(long photoId){
        final byte[] password = this.getUserPassword();
        ByteBuffer buffer = ByteBuffer.allocate(Long.BYTES);
        buffer.putLong(photoId);
        final byte[] id = buffer.array();
        byte[] key = password;
        for(int i=0; i < 4 ; i++){
            key[key.length-(5-i)] = id[i+3];
        }
        return new SecretKeySpec(this.hashPassword(key), "AES");
    }

    private byte[] encrypt(byte[] data, long photoId){
        SecretKeySpec key = generateKey(photoId);
        byte[] value = {};
        try {
            Cipher c = Cipher.getInstance("AES");
            c.init(Cipher.ENCRYPT_MODE, key);
            value = c.doFinal(data);
            Log.i("Encrypt Photo", "Photo Encrypted.");

        } catch (Exception e) {
            e.printStackTrace();
        }
        return value;
    }

    private byte[] decrypt(byte[] data, long photoId){
        SecretKeySpec key = generateKey(photoId);
        byte[] value = {};
        try {
            Cipher c = Cipher.getInstance("AES");
            c.init(Cipher.DECRYPT_MODE, key);
            value = c.doFinal(data);
            Log.i("Decrypt Photo", "Photo Decrypted.");

        } catch (Exception e) {
            e.printStackTrace();
        }
        return value;
    }

    public boolean canAccessVault(String password) {
        return Arrays.equals(this.hashPassword(password), this.getUserPassword());
    }

    public void createVault(String password){
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(USER_PASSWORD, this.hashPassword(password));

        // Inserting Row
        db.insert(TABLE_USER, null, values);

        db.close();
    }

    public void deleteUserPassword(){
        SQLiteDatabase db = this.getWritableDatabase();

        db.delete(TABLE_USER, null, null);

        db.close();
    }

    public void changeVaultPassword(String password){
        ArrayList<PV_Photo> photos = this.getAllPhotos();
        deleteUserPassword();
        createVault(password);

        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();

        for (PV_Photo photo: photos){
            values.put(PHOTO_DATA, encrypt(photo.getImageBytes(), photo.getId()));

            db.update(TABLE_PHOTO, values, PHOTO_ID+"="+photo.getId(), null);
        }
        db.close();
    }

    private ArrayList<PV_Photo> getAllPhotos(){
        ArrayList<PV_Photo> photos = new ArrayList<>();

        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(TABLE_PHOTO, new String[] { PHOTO_DATA, PHOTO_ID }, null,
                null , null, null, null, null);

        while(cursor.moveToNext()){
            PV_Photo photo = new PV_Photo(decrypt(cursor.getBlob(0), cursor.getLong(1)), cursor.getLong(1));

            photos.add(photo);
        }

        return photos;
    }
}
