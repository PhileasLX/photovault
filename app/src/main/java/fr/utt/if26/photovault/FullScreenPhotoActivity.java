package fr.utt.if26.photovault;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;

public class FullScreenPhotoActivity extends AppCompatActivity {
    private DB_Persistance db;
    private PV_Photo photo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fullscreenphoto);

        db = new DB_Persistance(this, DB_Persistance.DATABASE_NAME, null, DB_Persistance.DATABASE_VERSION);

        final Intent intent = getIntent();
        final long photoId = intent.getLongExtra("PhotoID", 0);
        final Album album = new Album(intent.getStringExtra("AlbumName"), intent.getIntExtra("AlbumID", 0));
        photo = new PV_Photo(db.getPhoto(photoId), photoId);

        ImageView imageView = (ImageView) findViewById(R.id.imageView_fullscreen);
        imageView.setImageBitmap(photo.getImage());
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(FullScreenPhotoActivity.this, AlbumActivity.class);
                intent.putExtra("AlbumID", album.getId());
                intent.putExtra("AlbumName", album.getName());
                startActivity(intent);
            }
        });
    }
}
