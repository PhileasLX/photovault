package fr.utt.if26.photovault;

import android.util.Log;

import java.util.ArrayList;

public class Album {
    private ArrayList<PV_Photo> photos;
    private String name;
    private int id;

    public Album(String name){
        this.name = name;
        this.photos = new ArrayList<>();
        this.id = -1;
    }

    public Album(String name, int id){
        this.name = name;
        this.photos = new ArrayList<>();
        this.id = id;
    }

    public String getName(){
        return this.name;
    }

    public int getId() { return this.id; }

    public void setName(String name) { this.name = name; }

    public void addPhoto(PV_Photo photo){
        this.photos.add(photo);
    }

    public void deletePhoto(int position){
        Log.i("deletePhoto", "before -> photos.size(): " + photos.size());
        this.photos.remove(position);
        Log.i("deletePhoto", "after -> photos.size(): " + photos.size());
    }

    public void setPhotos(ArrayList<PV_Photo> photos){
        this.photos = photos;
    }

    public PV_Photo getPhoto(int position){
        return this.photos.get(position);
    }

    public ArrayList<PV_Photo> getPhotos(){
        return this.photos;
    }
}
