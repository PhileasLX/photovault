package fr.utt.if26.photovault;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

public class PV_Photo {
    private byte[] bytes;
    private Bitmap image;
    private long id;

    public PV_Photo(Bitmap bitmap){
        this.id = System.currentTimeMillis();
        this.image = bitmap;
        convertToBytes();
        convertToScreenSize();
    }

    public PV_Photo(byte[] data, long id){
        this.id = id;
        this.bytes = data;
        this.convertToBitmap();
    }

    private void convertToBitmap(){
        this.image = new BitmapFactory().decodeByteArray(bytes, 0, bytes.length);
    }

    private void convertToBytes(){
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        this.image.compress(Bitmap.CompressFormat.JPEG,100, baos);
        this.bytes = baos.toByteArray();
        try {
            baos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void convertToScreenSize(){
        BitmapFactory.Options opt = new BitmapFactory.Options();
        opt.inSampleSize = 4;
        // Converts pics in screen size pics to display fullscreen photos
        this.image = new BitmapFactory().decodeByteArray(bytes, 0, bytes.length, opt);
        convertToBytes();
    }

    public Bitmap getReducedImage(){
        BitmapFactory.Options opt = new BitmapFactory.Options();
        // Converts pics in Width:432 * Height:576 to optimize memory and photo quality
        opt.inSampleSize = 2;
        return new BitmapFactory().decodeByteArray(bytes, 0, bytes.length, opt);
    }

    public Bitmap getImage(){
        return this.image;
    }

    public byte[] getImageBytes(){ return this.bytes; }

    public long getId(){ return this.id; }
}
