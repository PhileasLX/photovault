package fr.utt.if26.photovault;

import android.app.Activity;
        import android.content.Context;
        import android.view.LayoutInflater;
        import android.view.View;
        import android.view.ViewGroup;
        import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

public class AlbumAdapter extends ArrayAdapter<Album> {
    ArrayList<Album> albums;
    Context contexte;
    int ressource;

    public AlbumAdapter(Context context, int resource, ArrayList<Album> data) {
        super(context, resource, data);
        this.albums = data;
        this.contexte = context;
        this.ressource = resource;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = ((Activity) contexte).getLayoutInflater();
        View v = inflater.inflate(ressource, parent, false);

        Album album = albums.get(position);

        TextView tv = (TextView) v.findViewById(R.id.tv_albumName);
        tv.setText(album.getName());

        return v;

    }
}
