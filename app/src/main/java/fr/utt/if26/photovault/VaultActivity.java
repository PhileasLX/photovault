package fr.utt.if26.photovault;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.Toast;

import java.util.ArrayList;

public class VaultActivity extends AppCompatActivity {
    private Menu menu;
    private DB_Persistance db;
    private ArrayList<Album> albums;
    private AlbumAdapter adapter;
    private ListView lv_vault;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vault);

        db = new DB_Persistance(this, DB_Persistance.DATABASE_NAME, null, DB_Persistance.DATABASE_VERSION);

        lv_vault = (ListView) findViewById(R.id.lv_vault);
        this.albums = db.getAlbums();
        adapter = new AlbumAdapter(this, R.layout.albumadapter, albums);
        lv_vault.setAdapter(adapter);

        registerForContextMenu(lv_vault);

        lv_vault.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                showAlertDialogAccessAlbum(position);
            }
        });

        lv_vault.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                final int itemPosition = position;
                PopupMenu popup = new PopupMenu(VaultActivity.this, lv_vault.getChildAt(position), Gravity.RIGHT);
                popup.getMenuInflater().inflate(R.menu.contextmenu_album, popup.getMenu());

                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.contextmenu_album_delete:
                                showAlertDialogDeleteAlbum(itemPosition);
                                return true;
                            case R.id.contextmenu_album_edit:
                                showAlertDialogEditAlbumChoice(itemPosition);
                                return true;

                            default:
                                return true;
                        }
                    }
                });
                popup.show();
                return true;
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_vault, menu);
        this.menu = menu;
        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(VaultActivity.this, MainActivity.class);
        startActivity(intent);
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_vault_addAlbum:
                this.showAlertDialogAddAlbum();
                return true;
            case R.id.menu_vault_changePassword:
                showAlertDialogChangeVaultPassword();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }

    }
    public void showAlertDialogChangeVaultPassword() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();

        final View dialogView = inflater.inflate(R.layout.dialog_changevaultpassword, null);

        builder.setView(dialogView);
        builder.setTitle("Changez le mot de passe du Coffre");

        final EditText et_oldPassword = (EditText) dialogView.findViewById(R.id.et_oldVaultPassword);
        final EditText et_newPassword = (EditText) dialogView.findViewById(R.id.et_newVaultPassword);
        final EditText et_confirmationPassword = (EditText) dialogView.findViewById(R.id.et_newVaultPasswordConfirmation);

        builder.setPositiveButton("Valider", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                final String oldPassword = et_oldPassword.getText().toString();
                final String newPassword = et_newPassword.getText().toString();
                final String confirmationPassword = et_confirmationPassword.getText().toString();
                if(db.canAccessVault(oldPassword)){
                    if(!newPassword.equals("")){
                        if(newPassword.equals(confirmationPassword)){
                            db.changeVaultPassword(newPassword);
                        }else
                            Toast.makeText(VaultActivity.this, "Mots de passe différents.", Toast.LENGTH_LONG).show();
                    }else
                        Toast.makeText(VaultActivity.this, "Entrez un nouveau mot de passe.", Toast.LENGTH_LONG).show();

                }
                else{
                    Toast.makeText(getApplicationContext(), "Mauvais mot de passe.", Toast.LENGTH_LONG).show();
                }

            }
        });

        builder.setNegativeButton("Annuler", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
            }
        });

        AlertDialog dialog = builder.create();
        dialog.show();
    }


    public void showAlertDialogDeleteAlbum(int id) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();

        final View dialogView = inflater.inflate(R.layout.dialog_askpassword, null);

        builder.setView(dialogView);
        builder.setTitle("Entrez le mot de passe");

        final EditText et_password = (EditText) dialogView.findViewById(R.id.et_askedPassword);

        final int itemPosition = id;
        final int albumId = albums.get(itemPosition).getId();


        builder.setPositiveButton("Valider", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
            if(db.canAccessAlbum(et_password.getText().toString(), albumId)) {
                db.deleteAlbum(albumId);
                Toast.makeText(getApplicationContext(), "Album " + albums.get(itemPosition).getName() + " supprimé.", Toast.LENGTH_LONG).show();
                albums.remove(itemPosition);
                adapter.notifyDataSetChanged();
                db.getAlbums();
            }
            else{
                Toast.makeText(getApplicationContext(), "Mauvais mot de passe.", Toast.LENGTH_LONG).show();
            }

                }
        });

        builder.setNegativeButton("Annuler", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
            }
        });

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    public void showAlertDialogEditAlbumChoice(int position) {
        // setup the alert builder
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Modifier l'album");
        final int itemPosition = position;

        // add the buttons
        builder.setPositiveButton("Changer le nom", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                showAlertDialogEditAlbum(itemPosition, db.EDIT_NAME);
            }
        });
        builder.setNegativeButton("Changer le mot de passe", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                showAlertDialogEditAlbum(itemPosition, db.EDIT_PASSWORD);
            }
        });

        // create and show the alert dialog
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    public void showAlertDialogEditAlbum(int id, short arg) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();

        final View dialogView = inflater.inflate(R.layout.dialog_albumadd, null);

        builder.setView(dialogView);

        final EditText et_name = (EditText) dialogView.findViewById(R.id.et_newAlbumName);
        final EditText et_password = (EditText) dialogView.findViewById(R.id.et_newAlbumPassword);

        final int itemPosition = id;
        final int albumId = albums.get(itemPosition).getId();

        if(arg == db.EDIT_NAME){
            et_name.setHint("Nouveau nom");
            et_password.setHint("Mot de passe");
            builder.setTitle("Modifier le nom");
            builder.setPositiveButton("Valider", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    final String newName = et_name.getText().toString();
                    if(!newName.equals("")) {
                        if (db.canAccessAlbum(et_password.getText().toString(), albumId)) {
                            Toast.makeText(getApplicationContext(), "Album " + albums.get(itemPosition).getName() + " modifié en " + newName, Toast.LENGTH_LONG).show();
                            db.editAlbum(albumId, newName, db.EDIT_NAME);
                            albums.remove(itemPosition);
                            albums.add(itemPosition, new Album(newName, albumId));
                        }else{
                            Toast.makeText(getApplicationContext(), "Mauvais mot de passe.", Toast.LENGTH_LONG).show();
                        }
                    }
                    else{
                        Toast.makeText(getApplicationContext(), "Entrez un nouveau nom.", Toast.LENGTH_LONG).show();
                    }

                }
            });
        }if(arg == db.EDIT_PASSWORD){
            et_name.setHint("Nouveau mot de passe");
            et_password.setHint("Mot de passe");
            et_name.setInputType(et_password.getInputType());
            builder.setTitle("Modifier le mot de passe");

            builder.setPositiveButton("Valider", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    final String newPass = et_name.getText().toString();
                    if (db.canAccessAlbum(et_password.getText().toString(), albumId)) {
                        Toast.makeText(getApplicationContext(), "Mot de passe changé.", Toast.LENGTH_LONG).show();
                        db.editAlbum(albumId, newPass, db.EDIT_PASSWORD);
                    }else{
                        Toast.makeText(getApplicationContext(), "Mauvais mot de passe.", Toast.LENGTH_LONG).show();
                    }



                }
            });
        }


        builder.setNegativeButton("Annuler", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
            }
        });

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    public void showAlertDialogAddAlbum() {
        // setup the alert builder
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.dialog_albumadd, null);
        builder.setView(dialogView);
        builder.setTitle("Ajouter un album");

        final EditText et_name = (EditText) dialogView.findViewById(R.id.et_newAlbumName);
        final EditText et_password = (EditText) dialogView.findViewById(R.id.et_newAlbumPassword);

        et_name.setHint("Nom de l'album");

        // add the buttons
        builder.setPositiveButton("Valider", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                final String albumName = et_name.getText().toString();
                if(!albumName.equals("")){
                    // db.addAlbum() returns the new row's id in db
                    final int albumId = db.addAlbum(new Album(albumName), et_password.getText().toString());
                    albums.add(new Album(albumName, albumId));
                    adapter.notifyDataSetChanged();
                    Intent intent = new Intent(VaultActivity.this, AlbumActivity.class);
                    intent.putExtra("AlbumID", albumId);
                    intent.putExtra("AlbumName", albumName);
                    startActivity(intent);
                }else
                    Toast.makeText(VaultActivity.this, "Entrez un nom à l'album.", Toast.LENGTH_LONG).show();

            }
        });
        builder.setNegativeButton("Annuler", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
            }
        });

        // create and show the alert dialog
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    public void showAlertDialogAccessAlbum(int id) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();

        final View dialogView = inflater.inflate(R.layout.dialog_askpassword, null);

        builder.setView(dialogView);
        builder.setTitle("Entrez le mot de passe");

        final EditText et_password = (EditText) dialogView.findViewById(R.id.et_askedPassword);

        final int itemPosition = id;
        final int albumId = albums.get(itemPosition).getId();


        builder.setPositiveButton("Valider", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                if(db.canAccessAlbum(et_password.getText().toString(), albumId)) {
                    Intent intent = new Intent(VaultActivity.this, AlbumActivity.class);
                    intent.putExtra("AlbumID", albumId);
                    intent.putExtra("AlbumName", albums.get(itemPosition).getName());
                    startActivity(intent);
                }
                else{
                    Toast.makeText(getApplicationContext(), "Mauvais mot de passe.", Toast.LENGTH_LONG).show();
                }

            }
        });

        builder.setNegativeButton("Annuler", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
            }
        });

        AlertDialog dialog = builder.create();
        dialog.show();
    }

}
